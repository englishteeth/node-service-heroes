const jwksClient = require('jwks-rsa');
const jws = require('jws');
const nJwt = require('nJwt');

var extractToken = (request, cb) => {
  const { authorization } = request.headers;
  if (!authorization) return cb("No authorization header");
  const [authType, token] = authorization.trim().split(' ');
  if (authType !== 'Bearer') return cb("No Bearer token in header");
  return cb(null, token);
};

var decodeToken = function (jwt, cb) {
  var decoded = jws.decode(jwt);
  if (!decoded) return cb("Unable to decode token");
  var payload = decoded.payload;

  //try parse the payload
  if(typeof payload === 'string') {
    try {
      var obj = JSON.parse(payload);
      if(obj !== null && typeof obj === 'object') {
        payload = obj;
      }
    } catch (e) { }
  }

  return cb(null, {
    header: decoded.header,
    payload: payload,
    signature: decoded.signature
  });
};

class JwtVerifier {
  constructor(options = {}) {
    options = options || {};
    const { alg = 'none' } = options;

    this.jwksClient = jwksClient({
      jwksUri: process.env.JWK_SET_URI,
      cache: true,
      cacheMaxAge: options.cacheMaxAge || (60 * 60 * 1000),
      cacheMaxEntries: 3,
      jwksRequestsPerMinute: options.jwksRequestsPerMinute || 10,
      rateLimit: true
    });
    this.verifier = nJwt.createVerifier().setSigningAlgorithm(alg).withKeyResolver((kid, cb) => {
      this.jwksClient.getSigningKey(kid, (err, key) => {
        return cb(err, key && (key.publicKey || key.rsaPublicKey));
      });
    });
  }

  verify(accessTokenString, cb) {
      this.verifier.verify(accessTokenString, (err, jwt) => {
        if (err) return cb(err.message);
        return cb(null, jwt);
      });
  }

}

class AuthError extends Error {
  constructor(err) {
    super();
    this.status = 401;
    this.message = err || "Unauthorized";
  }
}

module.exports = function auth() {
  return function (req, res, next) {
    extractToken(req, (err, token) => {
      if (err) return next(new AuthError(err));
      decodeToken(token, (err, decodedToken) => {
        if (err) return next(new AuthError(err)); 
        new JwtVerifier(decodedToken.header).verify(token, (err, verifiedToken) => {
          if (err) return next(new AuthError(err));
          return next(null, verifiedToken);
        })
      });
    });
  };
};
