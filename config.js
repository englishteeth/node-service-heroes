const env = require("dotenv");

if (env.config().error) {
  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

module.exports = {
  port: parseInt(process.env.PORT || '3000', 10),
  jwtSetUri: process.env.JWK_SET_URI
}
