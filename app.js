const env = require('./config');

const express = require('express');
const cors = require('cors');
const auth = require('./middleware/auth');

const app = express();

const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors());

app.use(auth("heroes"));

const routes = require('./routes');
app.use(routes);

app.use((err, req, res, next) => {
  const { status = 500, message } = err;
  res.status(status).json({
    status,
    message
  });
});

app.listen(env.port, () => console.log(`Example app listening at http://localhost:${env.port}`));

module.exports = app;