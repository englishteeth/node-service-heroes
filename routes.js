const services = require("./services");
const express = require('express');
const router = express.Router();

router.get('/api/heroes', (req, res) => {
  services.getHeroes((err, heroes) => {
    if(err) return console.error(err);
    res.json(heroes)
  });
});

router.get('/api/heroes/:id', (req, res) => {
  const { id } = req.params;
  services.findHero(id, (err, hero) => {
    if(err) return console.error(err);
    res.json( (!hero) ? {} : hero );
  });
});

router.post('/api/heroes', (req, res) => {
  const { body:hero } = req;
  services.createHero(hero, (err, hero) => {
    if(err) return console.error(err);
    res
      .status(201)
      .set('Location', '/api/heroes/' + hero.id)
      .json({created: hero});
  });
});

module.exports = router;