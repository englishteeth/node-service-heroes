const chai = require('chai');
const httpMocks = require("node-mocks-http");
const sinon = require('sinon');
const services = require('../services');
const routes = require('../routes');

const expect = chai.expect;

describe('Routes ', function() {
	it('Should exists', function() {
    expect(routes).to.be.a('function');
  });

});

describe('Get Heroes', function() {
	it('Should return all the heroes', function(done) {
		const mockHeroes = [{id: 666, name: "Constantine"},{id: 999, name: "Plod"}];
		const stub =	sinon.stub(services, "getHeroes");
		stub.yields(null, mockHeroes);

		const mockRequest = httpMocks.createRequest({
      method: "GET",
      url: "/api/heroes"
    });
		const mockResponse = httpMocks.createResponse();
		
		routes(mockRequest, mockResponse);

		const status  = mockResponse._getStatusCode();
		const body  = mockResponse._getJSONData();

		expect(status).to.be.equal(200);
		expect(body).be.an('array')
		expect(body).to.deep.include.members(mockHeroes);

		stub.restore();
		done();
	});

	it('Should return a specific hero', function(done) {
		const mockHero = {id: 666, name: "Constantine"};
		const stub =	sinon.stub(services, "findHero");
		stub.withArgs("666", sinon.match.any).yields(null, mockHero);

		const mockRequest = httpMocks.createRequest({
      method: "GET",
      url: "/api/heroes/666"
    });
		const mockResponse = httpMocks.createResponse();
		
		routes(mockRequest, mockResponse);

		const status  = mockResponse._getStatusCode();
		const body  = mockResponse._getJSONData();

		expect(status).to.be.equal(200);
		expect(body).to.be.an('object');
		expect(body).to.include(mockHero);

		stub.restore();
		done();
	});

	it('Should not get 404 for hero not found', function(done) {
		const stub =	sinon.stub(services, "findHero");
		stub.withArgs("111", sinon.match.any).yields(null, null);

		const mockRequest = httpMocks.createRequest({
      method: "GET",
      url: "/api/heroes/111"
    });
		const mockResponse = httpMocks.createResponse();
		
		routes(mockRequest, mockResponse);

		const status  = mockResponse._getStatusCode();
		const body  = mockResponse._getJSONData();

		expect(status).to.be.equal(200);
		expect(body).to.be.an('object');
		expect(body).to.be.empty;

		stub.restore();
		done();
	});

});


describe('Hero creation', function() {
	it('Should accept POST', function(done) {
		const new_hero = { "name": "Klaus Wunderlich" };

		const stub =	sinon.stub(services, "createHero");
		stub.withArgs(new_hero, sinon.match.any).yields(null, {id:423, ...new_hero});

		const mockRequest = httpMocks.createRequest({
      method: "POST",
			url: "/api/heroes/",
			body: new_hero
    });
		const mockResponse = httpMocks.createResponse();

		routes(mockRequest, mockResponse);

		const status  = mockResponse._getStatusCode();
		const { location } = mockResponse._getHeaders();

		expect(status).to.be.equal(201);
		expect(location).to.be.equal("/api/heroes/423");

		stub.restore();
		done();
	});

})

