const chai = require('chai');
const services = require('../services');

const expect = chai.expect;

describe('Services ', function() {
	it('Should exists', function() {
    expect(services).to.be.an('object');
  });

});

describe('Heroes', function() {
	it('Should return all the heroes', function(done) {
		// Since it's currently a fixed array
		services.getHeroes((err, heroes) => {
			expect(err).to.be.null;
			expect(heroes).to.be.an('array').with.length(5);
		})
		done();
	});

	it('Should find a specific hero by id', function(done) {
		services.findHero(1, (err, hero) => {
			expect(err).to.be.null;
			expect(hero).to.be.an('object');
			expect(hero).to.have.property('id', 1);
			expect(hero).to.have.property('name', 'Bowie');
		})
		done();
	});

	it('Should handle not finding a specific hero by id', function(done) {
		// Not a great handling 
		services.findHero(100, (err, hero) => {
			expect(err).to.be.null;
			expect(hero).to.be.empty;
		})
		done();
	});

	it('Should find a specific hero by name', function(done) {
		services.findHeroByName("smith", (err, hero) => {
			expect(err).to.be.null;
			expect(hero).to.be.an('object');
			expect(hero).to.have.property('id', 4);
			expect(hero).to.have.property('name', 'Smith');
		})
		done();
	});

	it('Should handle not finding a specific hero by name', function(done) {
		// Not a great handling 
		services.findHeroByName("Captain Caveman", (err, hero) => {
			expect(err).to.be.null;
			expect(hero).to.be.empty;
		})
		done();
	});

	it('Should create a hero', function(done) {
		const candidate = { name: "Ash Williams"};
		services.createHero(candidate, (err, hero) => {
			expect(err).to.be.null;
			expect(hero).to.be.an('object');
			expect(hero).to.have.property('id', 6);
			expect(hero).to.have.property('name', candidate.name);
		});
		services.getHeroes((err, heroes) => {
			expect(err).to.be.null;
			expect(heroes).to.be.an('array').with.length(6);
		})
		done();
	});

	it('Should remove a hero', function(done) {
		services.removeHero(5, (err, hero) => {
			expect(err).to.be.null;
			expect(hero).to.be.an('object');
			expect(hero).to.have.property('id', 5);
			expect(hero).to.have.property('name', 'Pop');
		});
		services.getHeroes((err, heroes) => {
			expect(err).to.be.null;
			expect(heroes).to.be.an('array').with.length(5);
		})
		done();
	});

});

