const chai = require('chai');
const assert = require('assert');
const expect = chai.expect;

const sinon = require('sinon');
const nJwt = require('nJwt');

const auth = require('../middleware/auth');

describe('Auth ', () => {
	it('Should exists', () => {
    expect(auth).to.be.a('function');
  });
});

describe('Extracting the token', () => {
  it('Authorization header should be present', () => {
    const req = { body: {}, headers: {} };
    auth()(req,req,(error, result) => {
      expect(error).to.be.an('error').and.to.include({message: 'No authorization header', status: 401});
      expect(result).to.be.undefined;
    });
  });

  it('Bearer token should be in the authorization header', () => {
    const req = { body: {}, headers: { authorization: "nonsense" } };
    auth()(req,req,(error, result) => {
      expect(error).to.be.an('error').and.to.include({message: 'No Bearer token in header', status: 401});
      expect(result).to.be.undefined;
    });
  });

  it("Should be an error if not able to decode a token", () => {
    const req = { body: {}, headers: { authorization: "Bearer TheActualTokenGoesHere" } };
    auth()(req,req,(error, result) => {
      expect(error).to.be.an('error').and.to.include({message: 'Unable to decode token', status: 401});
      expect(result).to.be.undefined;
    });
  });

});

describe('Verifying the token', () => {
  const sandbox = sinon.createSandbox();

  beforeEach(function() {
    sandbox.spy(nJwt, 'createVerifier');
  });

  afterEach(function() {
      sandbox.restore();
  });

  it("Should set the correct token signing algorithm", () => {
    const req = { body: {}, headers: { authorization: EXPIRED_BEARER_TOKEN } };
    auth()(req,req,(error, result) => {
      assert(nJwt.createVerifier.calledOnce);
      expect(nJwt.createVerifier.getCall(0).returnValue).to.have.property('signingAlgorithm', 'RS256');
    });
  });

  it("Should not verify an expired token", () => {
    const req = { body: {}, headers: { authorization: EXPIRED_BEARER_TOKEN } };
    auth()(req,req,(error, result) => {
      expect(error).to.be.an('error').and.to.include({message: 'Jwt is expired', status: 401});
      expect(!result);
    });
  });

});

const EXPIRED_BEARER_TOKEN = 'Bearer eyJraWQiOiJqVnluZDNEWk52dFZSeEtMV0FjbUFzRFRTaHVTdndFWVItX3ZIRG1Oclo4IiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULmNrYzVBWEVxaGpEdXlXNFJqVi0xa1huQUNJcTcyWl9JNTVZYjRGT2RlbDQiLCJpc3MiOiJodHRwczovL2Rldi0yNzAxNzYub2t0YS5jb20vb2F1dGgyL2RlZmF1bHQiLCJhdWQiOiJhcGk6Ly9kZWZhdWx0IiwiaWF0IjoxNTg4MzU5MzMwLCJleHAiOjE1ODgzNjI5MzAsImNpZCI6IjBvYTJ5czE3YkRWOU9qdk9ONHg2IiwidWlkIjoiMDB1Mm9lNTlkbnk1OTJjT2g0eDYiLCJzY3AiOlsiaGVyb2VzIiwib3BlbmlkIl0sInN1YiI6ImlhbkBlbmdsaXNodGVldGguY29tIn0.W40q8mX4fJWg6pV1gDd_ERsUCFpwDI5aOhUMczYzKLZMT88zAR5A9I8oO5LXqbiU23GIQtqAn-vBVV0FhjrOoxFMR8zaKkcE-m-VXakrHozNdEjXTQwCyR7A9DRv4vso-KPg45lR7kk-VStB2zjlJEAXWJGQiNb_hoTuwqToewK57p-I16cIZNAl7sjbl4HQQBgsP0ROZk0PCFQSuHB3pKceoxqELnZW_pm5M-aXNoen39TUUD8hu7ZqyqrRd4vmq140YCGEVHqEZ7RoT0g88FSAPeB67FSzlDDl-MfvEjdt5yzxe5Z0Nq9yKyytECkgargULbC384PK4YkvLW-CdQ';
