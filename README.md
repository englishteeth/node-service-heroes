# NodeJS Heroes Service

Simple implementation of a backing service to the Angular Tour of Heroes tutorial implemmented in NodeJS and Express.

This service has been secured, expecting a valid Json Web Token in order access the api end points.

This is intended to demonstrate close to the minimum necessary code and configuration required to lock down the api. 

The service is secured by an external or third party OAuth provider. A situation you might expect to in _real world situation_. There is no provision for signing in. That is anticipated to be the responsibility of the client.

There are no secret keys in the configuration. Signing verification is routed back to the OAuth provider by specifying the `JWK_SET_URI`  in the `.env` file.

```
JWK_SET_URI ='jwk-set-uri: https://your-subdomain.okta.com/oauth2/v1/keys'
```

The service is secured by use of anthentication middleware, defining a security group scope that is expected in the validated token.

Apart from the minimal security set up, I wanted to also avoid using any vendor specific packages. For instance, although I used Okta working through this, the [Okta JWT Verifier for Node.js](https://github.com/okta/okta-oidc-js/tree/master/packages/jwt-verifier) is a little (not unreasonably) opinionated for their own service. Also I found neither [njwt](https://github.com/jwtk/njwt) nor [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) did everything I wanted. As a result I ended up bbasing my solution on [njwt](https://github.com/jwtk/njwt) and lifting a few bits from the other two packages.

