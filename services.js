// Heroes ... all heroes
let heroes = [
	{
		id:   1,
		name: "Bowie",
	},
	{
		id:   2,
		name: "Cope",
	},
	{
		id:   3,
		name: "Gira",
	},
	{
		id:   4,
		name: "Smith",
	},
	{
		id:   5,
		name: "Pop",
	},
];

var getHeroes = function(next) {
	next(null, heroes);
};

var findHero = function(id, next) {
  const hero = heroes.reduce( (a, h) => {
    return a = (h.id == id) ? h : a;
  }, "");
  next(null, hero);
};

var findHeroByName = function(name, next) {
	const match = new RegExp(name.toLowerCase(),'g');
  const hero = heroes.reduce( (a, h) => {
    return a = (h.name.toLowerCase().match(match)) ? h : a;
  }, "");
  next(null, hero);
};

var createHero = function(hero, next) {
	let candidate = {id: heroes.length + 1, ...hero};
	heroes.push(candidate);
	next(null, candidate);
};

var removeHero = function(id, next) {
	findHero(id, (err, hero) => {
		if(err) return console.error(err);
		heroes = heroes.filter(h => h.id !== id);
		next(null, hero);
	}); 
}

module.exports = { 
	getHeroes, 
	findHero, 
	findHeroByName, 
	createHero, 
	removeHero 
};